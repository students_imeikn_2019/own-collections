package students.collections.own;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList implements List, Deque {
    private Node first;
    private Node last;
    private int size;

    private static class Node {
        Node prev;
        Node next;
        Object item;
    }

    @Override
    public void addFirst(Object item) {
        Node newNode = new Node();
        newNode.item = item;
        ++size;

        if (first != null) {
            newNode.next = first;
            first.prev = newNode;
            first = newNode;
        } else {
            first = last = newNode;
        }
    }

    @Override
    public void addLast(Object item) {

    }

    @Override
    public Object getFirst() {
        return null;
    }

    @Override
    public Object getLast() {
        return null;
    }

    @Override
    public Object pollFirst() {
        return null;
    }

    @Override
    public Object pollLast() {
        return null;
    }

    @Override
    public Object removeFirst() {
        checkForNotEmpty();
        Object item = first.item;
        deleteNode(first);
        return item;
    }

    private void checkForNotEmpty() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public Object removeLast() {
        return null;
    }

    @Override
    public void add(int index, Object item) {

    }

    @Override
    public void set(int index, Object item) {
        checkForIndex(index);
        getNodeByIndex(index).item = item;
    }

    @Override
    public Object get(int index) {
        checkForIndex(index);
        return getNodeByIndex(index).item;
    }

    private void checkForIndex(int index) {
        if ((index < 0) || (index >= size)) {
            throw new IndexOutOfBoundsException();
        }
    }

    private Node getNodeByIndex(int index) {
        if (index <= size / 2) {
            Node current = first;
            for (int i = 0; i < index; ++i) {
                current = current.next;
            }
            return current;
        } else {
            Node current = last;
            for (int i = size - 1; i > index; --i) {
                current = current.prev;
            }
            return current;
        }
    }

    @Override
    public int indexOf(Object item) {
        if (item != null) {
            int index = 0;
            for (Node current = first; current != null; current = current.next) {
                if (item.equals(current.item)) {
                    return index;
                }
                ++index;
            }
        } else {
            int index = 0;
            for (Node current = first; current != null; current = current.next) {
                if (current.item == null) {
                    return index;
                }
                ++index;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object item) {
        return 0;
    }

    @Override
    public void remove(int index) {
        checkForIndex(index);
        deleteNode(getNodeByIndex(index));
    }

    private void deleteNode(Node deletingNode) {

    }

    @Override
    public List subList(int from, int to) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object item) {
        return false;
    }

    @Override
    public boolean add(Object item) {
        return false;
    }

    @Override
    public boolean remove(Object item) {
        return false;
    }

    @Override
    public void clear() {

    }

    private class ListIterator implements Iterator {
        private Node current = first;

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            return null;
        }
    }

    @Override
    public Iterator iterator() {
        return new ListIterator();
    }
}
