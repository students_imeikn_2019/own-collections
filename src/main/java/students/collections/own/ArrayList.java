package students.collections.own;

import java.util.Iterator;

public class ArrayList implements List {
    private Object[] array;
    private int size;

    public ArrayList() {
        this(10);
    }

    public ArrayList(int capacity) {
        array = new Object[capacity];
    }

    private void extendArrayAsNeeded() {
        if (size == array.length) {
            int newSize = array.length + array.length >> 1 + 1;
            Object[] newArray = new Object[newSize];
            for (int i = 0; i < array.length; ++i) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
    }

    @Override
    public boolean add(Object item) {
        extendArrayAsNeeded();
        array[size++] = item;
        return true;
    }

    @Override
    public void add(int index, Object item) {
        LinkedList list = new LinkedList();
        extendArrayAsNeeded();
        for (int i = size; i > index; --i) {
            array[i] = array[i - 1];
        }
        array[index] = item;
    }

    @Override
    public void set(int index, Object item) {

    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public int indexOf(Object item) {
        if (item != null) {
            for (int i = 0; i < size; ++i) {
                if (item.equals(array[i])) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; ++i) {
                if (array[i] == null) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object item) {
        return 0;
    }

    @Override
    public void remove(int index) {

    }

    @Override
    public List subList(int from, int to) {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object item) {
        return indexOf(item) != -1;
    }

    @Override
    public boolean remove(Object item) {
        return false;
    }

    @Override
    public void clear() {

    }

    private class ListIterator implements Iterator {
        private int current;

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            return null;
        }
    }

    @Override
    public Iterator iterator() {
        return new ListIterator();
    }
}
