package students.collections.own;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DequeTest {
    @DataProvider
    public Object[][] getQueues() {
        return new Object[][]{
                {new LinkedList()}
        };
    }

    @Test(dataProvider = "getQueues")
    public void addFirst(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void addLast(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void getFirst(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void getLast(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void pollFirst(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void pollLast(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void removeFirst(Deque deque) {
    }

    @Test(dataProvider = "getQueues")
    public void removeLast(Deque deque) {
    }
}