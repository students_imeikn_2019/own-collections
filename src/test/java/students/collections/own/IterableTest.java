package students.collections.own;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;

import static org.testng.Assert.assertEquals;

public class IterableTest {
    private static int ITEMS_COUNT = 10;

    @DataProvider
    public Object[][] getEmptyIterables() {
        return new Object[][]{
                {new ArrayList()},
                {new LinkedList()}
        };
    }

    @DataProvider
    public Object[][] getNotEmptyIterables() {
        return new Object[][]{
                {getListWithItems(new ArrayList())},
                {getListWithItems(new LinkedList())}
        };
    }

    private List getListWithItems(List list) {
        for (int i = 0; i < ITEMS_COUNT; ++i) {
            list.add("someString" + i);
        }
        return list;
    }

    @Test(dataProvider = "getEmptyIterables")
    public void iterator_ForEach_EmptyCase(Iterable iterable) throws Exception {
        int count = 0;
        for (Object item : iterable) {
            count++;
        }
        assertEquals(count, 0);
    }

    @Test(dataProvider = "getEmptyIterables")
    public void iterator_While_EmptyCase(Iterable iterable) throws Exception {
        int count = 0;
        Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Object item = iterator.next();
            count++;
        }
        assertEquals(count, 0);
    }

    @Test(dataProvider = "getNotEmptyIterables")
    public void iterator_ForEach_NotEmptyCase(Iterable iterable) throws Exception {
        int count = 0;
        for (Object item : iterable) {
            count++;
        }
        assertEquals(count, ITEMS_COUNT);
    }

    @Test(dataProvider = "getNotEmptyIterables")
    public void iterator_While_NotEmptyCase(Iterable iterable) throws Exception {
        int count = 0;
        Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Object item = iterator.next();
            count++;
        }
        assertEquals(count, ITEMS_COUNT);
    }
}