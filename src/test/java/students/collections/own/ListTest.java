package students.collections.own;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ListTest {
    @DataProvider
    public Object[][] getLists() {
        return new Object[][]{
                {new ArrayList()},
                {new LinkedList()}
        };
    }

    @Test(dataProvider = "getLists")
    public void add(List list) throws Exception {
        System.out.println(list.size());
    }

    @Test(dataProvider = "getLists")
    public void set(List list) throws Exception {
    }

    @Test(dataProvider = "getLists")
    public void get(List list) throws Exception {
    }

    @Test(dataProvider = "getLists", expectedExceptions = IndexOutOfBoundsException.class)
    public void get_WhenListIsEmpty(List list) throws Exception {
        list.get(0);
    }

    @Test(dataProvider = "getLists")
    public void indexOf(List list) throws Exception {
    }

    @Test(dataProvider = "getLists")
    public void lastIndexOf(List list) throws Exception {
    }

    @Test(dataProvider = "getLists")
    public void remove(List list) throws Exception {
    }

    @Test(dataProvider = "getLists")
    public void subList(List list) throws Exception {
    }
}