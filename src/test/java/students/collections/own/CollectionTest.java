package students.collections.own;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CollectionTest {
    @DataProvider
    public Object[][] getCollections() {
        return new Object[][]{
                {new ArrayList()},
                {new LinkedList()}
        };
    }

    @Test(dataProvider = "getCollections")
    public void size(Collection collection) throws Exception {
    }

    @Test(dataProvider = "getCollections")
    public void isEmpty(Collection collection) throws Exception {
    }

    @Test(dataProvider = "getCollections")
    public void contains(Collection collection) throws Exception {
    }

    @Test(dataProvider = "getCollections")
    public void clear(Collection collection) throws Exception {
    }

    @Test(dataProvider = "getCollections")
    public void add(Collection collection) throws Exception {
        String addingString = "testString";
        collection.add(addingString);

        assertEquals(collection.size(), 1);
        assertTrue(collection.contains(addingString));
    }

    @Test(dataProvider = "getCollections")
    public void remove(Collection collection) throws Exception {
        collection.add("testString1");

        collection.add("testString2");
        collection.add("testString2");

        collection.add("testString3");
        collection.add("testString3");
        collection.add("testString3");

        assertTrue(collection.remove("testString3"));
        assertEquals(collection.size(), 3);

        assertTrue(collection.remove("testString2"));
        assertEquals(collection.size(), 1);

        assertTrue(collection.remove("testString1"));
        assertEquals(collection.size(), 0);
    }
}